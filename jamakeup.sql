-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Abr-2019 às 18:51
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jamakeup`
--
CREATE DATABASE IF NOT EXISTS `jamakeup` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `jamakeup`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atendimentos`
--

CREATE TABLE IF NOT EXISTS `atendimentos` (
  `id` int(11) NOT NULL,
  `idOrcamento` int(11) NOT NULL,
  `dataAge` date NOT NULL,
  `nome` varchar(60) NOT NULL,
  `enderecoAtendimento` varchar(100) NOT NULL,
  `servico` varchar(40) NOT NULL,
  `horaAgendada` char(5) NOT NULL,
  `valorDeslocamento` float NOT NULL,
  `faltaReceber` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `financeiro`
--

CREATE TABLE IF NOT EXISTS `financeiro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL,
  `dataMovimento` date NOT NULL,
  `tipoMovimento` char(7) NOT NULL,
  `valorMovimento` float NOT NULL,
  `codAtendimento` int(11) NOT NULL,
  `meioPagamento` char(12) NOT NULL,
  `dataLiberacaoPagSeguro` char(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamentos`
--

CREATE TABLE IF NOT EXISTS `orcamentos` (
  `id` int(11) NOT NULL,
  `nomeCliente` varchar(60) NOT NULL,
  `telefone` char(9) NOT NULL,
  `enderecoAtendimento` varchar(100) NOT NULL,
  `dataOrcamento` date NOT NULL,
  `formaContato` char(9) NOT NULL,
  `tipoEvento` varchar(20) NOT NULL,
  `tipoParticipacao` varchar(25) NOT NULL,
  `diaHora` char(16) NOT NULL,
  `servicoSolicitado` int(11) NOT NULL,
  `horaAgendada` char(5) NOT NULL,
  `deslocamento` float NOT NULL,
  `valorTotal` float NOT NULL,
  `taxaReserva` float NOT NULL,
  `formaPagamento` char(13) NOT NULL,
  `statusOrcamento` char(9) NOT NULL,
  `obsOrcamento` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeServico` varchar(60) NOT NULL,
  `descricaoServico` varchar(60) NOT NULL,
  `valor` float NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
