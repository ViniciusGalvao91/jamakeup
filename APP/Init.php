<?php

namespace APP;

use SON\Init\Bootstrap;

class Init extends Bootstrap{
    
    /**aqui colocamos as rotas que podem ser acessadas / encaminhamento para os controllers models ou view
    */ 
    protected function initRoutes(){
        
        //rota / = index
        $ar['home'] = array('route'=>'/','controller'=>'index','action'=>'index'); 
        
        //rotas para orçamentos
        $ar['orcamentos'] = array('route'=>'/orcamentos','controller'=>'orcamentos','action'=>'orcamentos');
        
        $ar['orcamentos/salvar'] = array('route'=>'/orcamentos/salvar','controller'=>'orcamentos','action'=>'salvar');
        
        $ar['orcamentos/Novo'] = array('route'=>'/orcamentos/Novo','controller'=>'orcamentos','action'=>'Novo_Orcamento');
        
        $ar['orcamentos/Listar'] = array('route'=>'/orcamentos/Listar','controller'=>'orcamentos','action'=>'Listar_Orcamentos');
        
        $ar['orcamentos/Alterar'] = array('route'=>'/orcamentos/Alterar','controller'=>'orcamentos','action'=>'Alterar_Orcamento');
        
        $ar['orcamentos/Consulta'] = array('route'=>'/orcamentos/Consulta','controller'=>'orcamentos','action'=>'consulta');
        
        //rotas para clientes
        $ar['clientes'] = array('route'=>'/clientes','controller'=>'cliente','action'=>'cliente');        
        
        $ar['clientes/pesquisa'] = array('route'=>'/clientes/pesquisa','controller'=>'cliente','action'=>'pesquisa');
        
        $ar['clientes/pesquisaConsulta'] = array('route'=>'/clientes/pesquisaConsulta','controller'=>'cliente','action'=>'pesquisaConsulta');
        
        $ar['clientes/cadastro'] = array('route'=>'/clientes/cadastro','controller'=>'cliente','action'=>'cadastro');
        
        $ar['clientes/inserirCliente'] = array('route'=>'/clientes/inserirCliente','controller'=>'cliente','action'=>'inserirCliente');
        
        $ar['clientes/editar'] = array('route'=>'/clientes/editar','controller'=>'cliente','action'=>'editar');
        
        $ar['clientes/excluir'] = array('route'=>'/clientes/excluir','controller'=>'cliente','action'=>'excluir');
        
        $ar['clientes/cadastro/cadastrarCliente'] = array('route'=>'/clientes/cadastro/cadastrarCliente','controller'=>'cliente','action'=>'cadastrarCliente');
        
        $ar['clientes/pesquisa/pesquisa'] = array('route'=>'/clientes/pesquisa/pesquisa','controller'=>'cliente','action'=>'pesquisa');
        
        //rotas para atendimentos
        $ar['atendimentos'] = array('route'=>'/atendimentos','controller'=>'atendimentos','action'=>'atendimentos');
        
        $ar['atendimentos/Novo'] = array('route'=>'/atendimentos/Novo','controller'=>'atendimentos','action'=>'Novo_Atendimento');
        
        $ar['atendimentos/salvar'] = array('route'=>'/atendimentos/salvar','controller'=>'atendimentos','action'=>'salvar');
        
        $ar['atendimentos/Listar'] = array('route'=>'/atendimentos/Listar','controller'=>'atendimentos','action'=>'Listar_Atendimento');
        
        $ar['atendimentos/Gerar_Atendimento'] = array('route'=>'/atendimentos/Gerar_Atendimento','controller'=>'atendimentos','action'=>'Gerar_Atendimento');
        
        $ar['atendimentos/Alterar_Atendimento'] = array('route'=>'/atendimentos/Alterar_Atendimento','controller'=>'atendimentos','action'=>'Alterar_Atendimento');
        
        $ar['atendimentos/Consulta'] = array('route'=>'/atendimentos/Consulta','controller'=>'atendimentos','action'=>'consulta');
        
        $ar['atendimentos/mapa'] = array('route'=>'/atendimentos/mapa','controller'=>'atendimentos','action'=>'mapa');
        
        //rotas para servicos
        $ar['servicos'] = array('route'=>'/servicos','controller'=>'servicos','action'=>'servicos'); 
        
        $ar['servicos/salvar'] = array('route'=>'/servicos/salvar','controller'=>'servicos','action'=>'salvar');
        
        $ar['servicos/Novo_Servico'] = array('route'=>'/servicos/Novo_Servico','controller'=>'servicos','action'=>'Novo_Servico');
        
        $ar['servicos/Listar_Servicos'] = array('route'=>'/servicos/Listar_Servicos','controller'=>'servicos','action'=>'Listar_Servicos');
        
        $ar['servicos/Alterar_Servico'] = array('route'=>'/servicos/Alterar_Servico','controller'=>'servicos','action'=>'Alterar_Servico');
        
        $ar['servicos/Consulta'] = array('route'=>'/servicos/Consulta','controller'=>'servicos','action'=>'Consulta');
        
        $ar['servicos/Consultajson'] = array('route'=>'/servicos/Consultajson','controller'=>'servicos','action'=>'Consultajson');
        
        //rotas para financeiro
        $ar['financeiro'] = array('route'=>'/financeiro','controller'=>'financeiro','action'=>'financeiro');
        
        $ar['financeiro/salvar'] = array('route'=>'/financeiro/salvar','controller'=>'financeiro','action'=>'salvar');
        
        $ar['financeiro/Novo_lancamento'] = array('route'=>'/financeiro/Novo_lancamento','controller'=>'financeiro','action'=>'Novo_lancamento');
        
        $ar['financeiro/extratos'] = array('route'=>'/financeiro/extratos','controller'=>'financeiro','action'=>'extratos');
        
        $ar['financeiro/listar'] = array('route'=>'/financeiro/listar','controller'=>'financeiro','action'=>'listar');
        
        $ar['financeiro/Alterar_Lancamento'] = array('route'=>'/financeiro/Alterar_Lancamento','controller'=>'financeiro','action'=>'Alterar_Lancamento');
        
        $ar['financeiro/Consulta'] = array('route'=>'/financeiro/Consulta','controller'=>'financeiro','action'=>'Consulta');
        
        //rotas para endereço
        $ar['enderecos'] = array('route'=>'/enderecos','controller'=>'index','action'=>'enderecos');
        $this->setRoutes($ar);
    }
    
    public static function getDb(){
        
                    
            $db = new \PDO("mysql:host=localhost;dbname=jamakeup","root","root");
            return $db;
       
    }
}