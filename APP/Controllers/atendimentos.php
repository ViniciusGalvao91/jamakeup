<?php

namespace APP\Controllers;

use SON\Controller\Action;
use \SON\Di\Container;


class Atendimentos extends Action{
        
    //envia para a tela o conteudo retornado da classe init
    public function Atendimentos(){
        
        $this->Novo_Atendimento();
    }
    
    public function Novo_Atendimento(){
        
        $atendimento = Container::getClass("Atendimentos");
        
        $this->view->atendimento = $atendimento->geraId();
  
        $this->render('Novo_Atendimento');  
        
    }
    
    public function Listar_Atendimento(){
                                         
        $atendimento = Container::getClass("Atendimentos");
        
        $atendimentos = $atendimento->getAtendimentos();
            
        $this->view->atendimentos = $atendimentos;
            
        $this->render('Listar_Atendimento');  
    }
    
    public function Mapa(){
                
        //action que desejo renderizar
        $this->render('mapa');
    }
               
    //envia para a tela o conteudo retornado da classe init
    public function salvar(){
             
        $atendimento = Container::getClass("Atendimentos");
        
        $atendimentos = $atendimento->salvar($_POST['id'], $_POST['idOrcamento'], $_POST['dataAge'], utf8_decode($_POST['nome']), utf8_decode($_POST['enderecoAtendimento']), utf8_decode($_POST['servico']), $_POST['horaAgendada'], $_POST['valorDeslocamento'], $_POST['faltaReceber']);  
        
        $this->view->atendimento = $atendimento->geraId();

        $this->Listar_Atendimento();
                
    }
    
    public function Gerar_Atendimento(){
                    
        $atendimento = Container::getClass("Atendimentos");
        
        $atendimentos = $atendimento->salvar($atendimento->geraId(), $_GET['idOrcamento'], $_GET['dataAge'], utf8_decode($_GET['nome']), utf8_decode($_GET['enderecoAtendimento']),  utf8_decode($_GET['servico']), $_GET['horaAgendada'], $_GET['valorDeslocamento'], $_GET['valorTotal'] - $_GET['taxaReserva']);  
        
        $this->Listar_Atendimento();
                
    }
            
    public function consulta(){
                               
        $atendimento = Container::getClass("Atendimentos");
        
        $result = $atendimento->getAtendimento($_GET['AtendimentoId']);
            
        //envia os dados para a view
        $this->view->atendimento = $result;
        
        $this->render('Alterar_Atendimento');
                    
    }
    
    
}