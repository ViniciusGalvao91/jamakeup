<?php

namespace APP\Controllers;

use SON\Controller\Action;
use \SON\Di\Container;


class Servicos extends Action{
        
    //envia para a tela o conteudo retornado da classe init
    public function Servicos(){
        
        //action que desejo renderizar
        //$this->render('Servicos');
        $this->Novo_Servico();
    }
    
    public function Novo_Servico(){
        
        
      $this->render('Novo_Servico');  
        
    }
    
    public function Listar_Atendimento(){
                                         
        $servico = Container::getClass("Atendimentos");
        
        $servicos = $servico->getAtendimentos();
            
        $this->view->servicos = $servicos;
            
        $this->render('Listar_Atendimento');  
    }
    
    public function Alterar_Servico(){
        
        
      $this->render('Alterar_Servico');  
        
    }
               
    public function salvar(){
             
        if(isset($_POST['status'])){
            
            $status = $_POST['status'];
             
        }else{
            
            $status = "0";
                    
        }
        
        $servico = Container::getClass("Servicos");

        $servicos = $servico->salvar($_POST['id'], utf8_decode($_POST['nomeServico']), utf8_decode($_POST['descricaoServico']), $_POST['valorServico'], $status);
                
        $this->Listar_Servicos();
                
    }
        
    public function Listar_Servicos(){
                                         
        $servico = Container::getClass("Servicos");
                    
        $this->view->servicos = $servico->getServicos();
            
        $this->render('listar');  
    }
    
    public function Consulta(){
        
        $id = $_GET["ServicoId"];
        
        $servico = Container::getClass("Servicos");
                
        $dado = $servico->listarServico($id);
        
        $this->view->servico = $dado;
       
        $this->render('Alterar_Servico');
    }
    
    public function Consultajson(){
        
        $id = $_GET["ServicoId"];
        
        $servico = Container::getClass("Servicos");
                
        $dado = $servico->listarServico($id);
        
        $this->view->servico = $dado;
       
        $retorno = array();
                    
        $retorno['valor'] = utf8_encode($dado->valor);
                
        echo json_encode($retorno); 
    }
         
}