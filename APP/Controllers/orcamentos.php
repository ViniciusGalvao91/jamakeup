<?php

namespace APP\Controllers;

use SON\Controller\Action;
use \SON\Di\Container;


class Orcamentos extends Action{
        
    //envia para a tela o conteudo retornado da classe init
    public function orcamentos(){      

        //action que desejo renderizar
        //$this->render('orcamentos');
        $this->Novo_Orcamento();
    }
    
     public function Novo_Orcamento(){
        
        //instancia orçamentos para saber o ultimo id 
        $orcamento = Container::getClass("Orcamentos");
        
        $this->view->orcamento = $orcamento->geraId(); 
         
        //instancia servicos para mostrar a descricao
        $servico = Container::getClass("Servicos");
         
        $this->view->servicos = $servico->getServicos();
         
        $this->render('Novo_Orcamento');  
        
    }
    
    public function Listar_Orcamentos(){
        
        $orcamento = Container::getClass("Orcamentos");
        
        $this->view->orcamentos = $orcamento->listarOrcamentos(); 
                  
        $this->render('Listar_Orcamento');  
        
    }
    
    public function Alterar_Orcamento(){
                  
        $this->render('Alterar_Orcamento');  
        
    }

    //envia para a tela o conteudo retornado da classe init
    public function salvar(){
                   
        $orcamento = Container::getClass("Orcamentos");
        
        if(!empty($_POST['dataOrcamento'])){
            
            $orcamentos = $orcamento->gravar($_POST['id'], $_POST['dataOrcamento'], utf8_decode($_POST['nomeCliente']), $_POST['telefone'], utf8_decode($_POST['enderecoAtendimento']), $_POST['formaContato'], $_POST['tipoEvento'], utf8_decode($_POST['tipoParticipacao']), $_POST['diaHora'], $_POST['servicoSolicitado'], $_POST['horaAgendada'], $_POST['deslocamento'], $_POST['valorTotal'], $_POST['taxaReserva'], utf8_decode($_POST['formaPagamento']), $_POST['statusOrcamento'], utf8_decode($_POST['obsOrcamento']));
            
            $this->Listar_Orcamentos();
            
        }else{
            
            $this->Novo_Orcamento();
        }                                            
                         
        
            
        
    }
    
    public function pesquisa(){
                               
                    
        $cliente = Container::getClass("Orcamentos");
        
        $clientes = $cliente->fetAll();        
        
        $this->view->clientes = $clientes;
        
        //renderizando
        $this->render('pesquisa');
    }
    
    public function consulta(){
                               
        $orcamento = Container::getClass("Orcamentos");
        
        $result = $orcamento->listarOrcamento($_GET['OrcamentoId']);
            
        //envia os dados para a view
        $this->view->orcamento = $result;
        
        //instancia servicos para mostrar a descricao
        $servico = Container::getClass("Servicos");
         
        $this->view->servicos = $servico->getServicos();
                                         
        //renderizando
        $this->render('Alterar_Orcamento');
            
    }
    
    public function editarCliente(){
       
        //renderizando
        $this->render('editar');
    }
    
    public function excluirCliente(){
       
        //renderizando
        $this->render('excluir');
    }
    
   
}