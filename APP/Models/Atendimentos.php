<?php

namespace APP\Models;

use SON\Db\Table;

use SON\Db\Config\TLogger;
use SON\Db\Config\TLoggerTXT;

use SON\Db\Ado\TTransaction;

class Atendimentos extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"
    const TABLENAME = 'atendimentos';    
    

    public function salvar($id, $idOrcamento, $dataAge, $nome, $enderecoAtendimento, $servico, $horaAgendada, $valorDeslocamento, $faltaReceber){
        
         try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Inserindo Atendimento");
         
            $atendimento = new Atendimentos;
            $atendimento->id = $id;
            $atendimento->idOrcamento = $idOrcamento;
            $atendimento->dataAge = $dataAge;
            $atendimento->nome = $nome;
            $atendimento->enderecoAtendimento = $enderecoAtendimento;        
            $atendimento->servico = $servico; 
            $atendimento->horaAgendada = $horaAgendada; 
            $atendimento->valorDeslocamento = $valorDeslocamento; 
            $atendimento->faltaReceber = $faltaReceber;   
                                   
            $retorno = array();
            
            if($atendimento->store()){
                
                $retorno["sucesso"] = true;
                $retorno["mensagem"] = "Atendimento inserido com sucesso.";
                
            }else{
                
                $retorno["sucesso"] = false;
                $retorno["mensagem"] = "Nenhum registro alterado.";
                
            }
                        
            echo json_encode($retorno);
                        
            TTransaction::log("**Registros inseridos com sucesso.");
            TTransaction::close();            
                
        }catch (Exception $e){

            echo '<b>ERRO</b>' . $e->getMessage();
            TTransaction::rollback();
        }
    }
       
    public function getAtendimentos(){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo Atendimentos");

            $atendimento = new Atendimentos();
                    
            $atendimentos = $atendimento->loadAll();

            return $atendimentos;    

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
    public function getAtendimento($id){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo Atendimentos");

            $atendimento = new Atendimentos();
                    
            $atendimentos = $atendimento->load($id);

            return $atendimentos;    

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
    public function geraId(){
    
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo Último ID");

            $atendimento = new Atendimentos();
                    
            $atendimentos = $atendimento->getLast();
                 
            if(empty($atendimentos)){
                
                $id = 1;
                return $id;
                
            }else{
                
                $id = $atendimentos + 1;
                return $id;
            }

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
    public function capturaIdOrcamento(){
    
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo ID Orçamento");

            $orcamento = new Orcamentos();
                    
            $orcamentos = $orcamento->getLast();
                 
            

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
    public function data($data){
        
        return date("d/m/Y", strtotime($data));
        
    }
    

}