<?php

namespace APP\Models;

use SON\Db\Table;

use SON\Db\Config\TLogger;
use SON\Db\Config\TLoggerTXT;

use SON\Db\Ado\TTransaction;

class Orcamentos extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"-
    const TABLENAME = 'orcamentos';   
        
    public function gravar ($id, $dataOrcamento, $nomeCliente, $telefone, $enderecoAtendimento, $formaContato, $tipoEvento, $tipoParticipacao, $diaHora, $servicoSolicitado, $horaAgendada, $deslocamento, $valorTotal, $taxaReserva, $formadePagamento, $statusOrcamento, $obsOrcamento){
        
         try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Inserindo Orcamento");
         
            $orcamento = new Orcamentos;
            
            $orcamento->id = $id; 
            $orcamento->nomeCliente = $nomeCliente; 
            $orcamento->telefone = $telefone;
            $orcamento->enderecoAtendimento = $enderecoAtendimento;
            $orcamento->dataOrcamento = $dataOrcamento;
            $orcamento->formaContato = $formaContato;
            $orcamento->tipoEvento = $tipoEvento;
            $orcamento->tipoParticipacao = $tipoParticipacao;
            $orcamento->diaHora = $diaHora;
            $orcamento->servicoSolicitado = $servicoSolicitado;
            $orcamento->horaAgendada = $horaAgendada;
            $orcamento->deslocamento = $deslocamento;
            $orcamento->valorTotal = $valorTotal;
            $orcamento->taxaReserva = $taxaReserva;
            $orcamento->formaPagamento = $formadePagamento;
            $orcamento->statusOrcamento = $statusOrcamento;
            $orcamento->obsOrcamento = $obsOrcamento;
                                               
            $retorno = array();
            
            if($orcamento->store()){
                
                $retorno["sucesso"] = true;
                $retorno["mensagem"] = "Orcamento inserido com sucesso.";
                
            }else{
                
                $retorno["sucesso"] = false;
                $retorno["mensagem"] = "Nenhuma Alteracao Encontrada";
                
            }
                        
            echo json_encode($retorno);
                        
            TTransaction::log("**Registros inseridos com sucesso.");
            TTransaction::close();            
                
        }catch (Exception $e){

            echo '<b>ERRO</b>' . $e->getMessage();
            TTransaction::rollback();
        }
        
    }
    
    /* falta implementar o controlo de ano, pois ao virar o ano ele não esta *    *  alaterando a contagem
     */
    public function geraId(){
    
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo Último ID");

            $orcamento = new Orcamentos();
                    
            $id = $orcamento->getLast();
            
            //separa somente o mes
            $mes = substr($id,4,2);
                 
            //verifica se existe dado na tabela se nao inicia a contagem
            if(empty($id)){
                
                $id = date("Ym") + $id . 1;
                return $id;
              
            //verifica se o mes ainda é o mesmo dos registros de orcamentos    
            }elseif($mes == date("m")){
                
                $id = $id + 1;
                
                return $id;
            
            //mudando o mes a contagem reinicia dentro do mes novo
            }else{
                
                $id = date("Ym") . 1;
                return $id;
            }

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
    public function data($data){
        
        return date("d/m/Y", strtotime($data));
        
    }
    
    public function dateTime($data){
        
        return date("d/m/Y H:i", strtotime($data));
        
    }
    
    
    
    public function listarOrcamentos(){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo Orçamentos");

            $orcamento = new Orcamentos();
             
            if($orcamento->loadAll()){
                
                $orcamentos = $orcamento->loadAll();
                
                return $orcamentos;  
            }

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
        
    }
    
    public function listarOrcamento($id){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo Orçamento");

            $orcamento = new Orcamentos();
             
            if($orcamento->load($id)){
                
                $result = $orcamento->load($id);
                
                return $result;
                
            }
            
            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
        
    }
}