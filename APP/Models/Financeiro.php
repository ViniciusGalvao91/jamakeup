<?php

namespace APP\Models;

use SON\Db\Table;

use SON\Db\Config\TLogger;
use SON\Db\Config\TLoggerTXT;

use SON\Db\Ado\TTransaction;

class Financeiro extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"-
    const TABLENAME = 'financeiro';   
        
    public function gravar($descricao, $dataMovimento, $tipoMovimento, $valorMovimento, $codAtendimento, $meioPagamento, $dataLiberacaoPagSeguro){
        
         try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Inserindo Movimento");
         
            $movimento = new Financeiro;
            $movimento->descricao = $descricao;
            $movimento->dataMovimento = $dataMovimento;
            $movimento->tipoMovimento = $tipoMovimento;
            $movimento->valorMovimento = $valorMovimento;
            $movimento->codAtendimento = $codAtendimento;
            $movimento->meioPagamento = $meioPagamento;
            $movimento->dataLiberacaoPagSeguro = $dataLiberacaoPagSeguro;
                                   
            $retorno = array();
            
            if($movimento->store()){
                
                $retorno["sucesso"] = true;
                $retorno["mensagem"] = "Movimento inserido com sucesso.";
                
            }else{
                
                $retorno["sucesso"] = false;
                $retorno["mensagem"] = "Falha ao inserir Movimento.";
                
            }
                        
            echo json_encode($retorno);
                        
            TTransaction::log("**Registros inseridos com sucesso.");
            TTransaction::close();            
                
        }catch (Exception $e){

            echo '<b>ERRO</b>' . $e->getMessage();
            TTransaction::rollback();
        }
    }
  
    public function getLancamentos(){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo Lançamentos");

            $lancamento = new Financeiro();
                    
            $lancamentos = $lancamento->loadAll();

            return $lancamentos;    

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
    public function getLancamento($id){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo Lançamentos");

            $lancamento = new Financeiro();
                    
            $lancamentos = $lancamento->load($id);

            return $lancamentos;    

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
    public function data($data){
        
        return date("d/m/Y", strtotime($data));
        
    }
}