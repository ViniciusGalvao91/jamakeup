<?php

namespace APP\Models;

use SON\Db\Table;

use SON\Db\Config\TLogger;
use SON\Db\Config\TLoggerTXT;

use SON\Db\Ado\TTransaction;

class Servicos extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"
    const TABLENAME = 'servicos';    
    

    public function salvar($id, $nomeservico, $descricaoServico, $valor, $status){
        
         try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Inserindo Serviço");
         
            $servico = new Servicos;
            $servico->id = $id;
            $servico->nomeServico = $nomeservico;
            $servico->descricaoServico = $descricaoServico;
            $servico->valor = $valor;
            $servico->status = $status; 
                                                
            $retorno = array();
            
            if($servico->store()){
                
                $retorno["sucesso"] = true;
                $retorno["mensagem"] = " Comando Realizado com sucesso.";
                
            }else{
                
                $retorno["sucesso"] = false;
                $retorno["mensagem"] = " Falha ao Alterar Servico.";
                
            }
                        
            echo json_encode($retorno);
                        
            TTransaction::log("**Registros inseridos com sucesso.");
            TTransaction::close();            
                
        }catch (Exception $e){

            echo '<b>ERRO</b>' . $e->getMessage();
            TTransaction::rollback();
        }
    }
       
    public function getServicos(){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo Serviços");

            $servico = new Servicos();
                    
            $servicos = $servico->loadAll();

            return $servicos;    

            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
    }
    
     public function listarServico($id){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Obtendo Serviço");

            $servico = new Servicos();
             
            if($servico->load($id)){
                
                $result = $servico->load($id);
                
                return $result;
                
            }
            
            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
        
    }
      
    public function delServico($id){
        
        try{
        
            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('../Vendor/SON/Db/tmp/log.txt'));

            TTransaction::log("**Deletando Serviço");

            $servico = new Servicos();
             
            if($servico->delete($id)){
                
                $result = $servico->delete($id);
                
                return $result;
                
            }
            
            TTransaction::close();    

            }catch (Exception $e){

                echo '<b>ERRO</b>' . $e->getMessage();
                TTransaction::rollback();
            }
        
    }
}