<?php

namespace APP\Models;

use SON\Db\Table;

class Endereco extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"
    protected $table = "endereco";   
    
    private $cep
    private $rua
    private $numero
    private $complemento
    private $bairro
    private $cidade
    
    public function __construct($cep, $rua, $numero, $complemento, $bairro, $cidade){

        $this->cep = $cep;
        $this->rua = $rua;
        $this->numero = $numero;
        $this->complemento = $complemento;
        $this->bairro = $bairro;
        $this->cidade = $cidade;
    }
    
    public function setCep($cep){
        
        $this->cep = $cep;
    }
    
     public function getCep(){
        
        $this->cep = $cep;
         
         return $this->cep;
    }
    
    public function setRua($rua){
        
        $this->rua = $rua;
    }
    
     public function getRua(){
        
        $this->rua = $rua;
         
         return $this->rua;
    }
    
    public function setNumero($numero){
        
        $this->numero = $numero;
    }
    
     public function getNumero(){
        
        $this->numero = $numero;
         
         return $this->numero;
    }
    
    public function setId_cliente($complento){
        
        $this->complento = $complento;
    }
    
     public function getComplento(){
        
        $this->complento= $complento;
         
         return $this->complento;
    }
    
    public function setBairro($bairro){
        
        $this->bairro = $bairro;
    }
    
     public function getBairro(){
        
        $this->bairro = $bairro;
         
         return $this->bairro;
    }
    
    public function setCidade($cidade){
        
        $this->cidade = $cidade;
    }
    
     public function getCidade(){
        
        $this->cidade = $cidade;
         
         return $this->cidade;
    }

}