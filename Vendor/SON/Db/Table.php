<?php

namespace SON\Db;

//TRECORD
use SON\Db\Ado\TTransaction;

use SON\Db\Ado\TSqlSelect;
use SON\Db\Ado\TSqlInsert;
use SON\Db\Ado\TSqlUpdate;
use SON\Db\Ado\TSqlDelete;
use SON\Db\Ado\TCriteria;
use SON\Db\Ado\TFilter;

use Exception;

abstract class Table{
    
    protected $data;
   
    
    public function __construct($id = NULL){
        
        if($id){
            
            $object = $this->load($id);
            if($object){
                
                $this->fromArray($object->toArray());
                
            } 
        } 
    }
    
    public function __clone(){
        
        unset($this->$id);
        
    }
    
    public function __set($prop, $value){
        
        if(method_exists($this, 'set_'.$prop)){
            
            call_user_func(array($this, 'set_'.$prop), $value);
            
        }else{
            
            if($value == NULL){
                
                unset($this->data[$prop]);
                
            }else{
                
                $this->data[$prop] = $value;
                
            } 
        } 
    }
    
    public function __get($prop){
        
        if(method_exists($this, 'get_'.$prop)){
            
            return call_user_func(array($this, 'get_'.$prop));
            
        }else{
            
            if(isset($this->data[$prop])){
                
                return $this->data[$prop];
                
            }
        } 
    }
          
    private function getEntity()       {
        
        $class = get_class($this);
        
        return constant("{$class}::TABLENAME");
        
    }
           
    public function fromArray($data){
        
        $this->data = $data;
        
    }
           
    public function toArray(){
        
        return $this->data;
        
    }
    
    protected function getLast(){
        
        if($conn = TTransaction::get()){
            
            $sql = new TSqlSelect;
            $sql->addColumn('max(ID) as ID');
            $sql->setEntity($this->getEntity());
            
            TTransaction::log($sql->getInstruction());
                                
            $result = $conn->Query($sql->getInstruction());
            
            $row = $result->fetch();
            
            return $row[0];
            
        }else{
            
            throw new Exception('Não há transação ativa!!');
            
        }
       
    }
           
    public function store(){
        
        if(empty($this->data['id']) or (!$this->load($this->id))){
                      
            //if(empty($this->data['id']))              {
                
                //$this->id = $this->getLast() + 1;
            //}
            
            //cria instrução insert
            $sql = new TSqlInsert;
            $sql->setEntity($this->getEntity());
            
            foreach ($this->data as $key => $value){
                
                $sql->setRowData($key, $this->$key);
                
            }
        }else{
            
            //cria instruçao update
            $sql = new TSqlUpdate;
            $sql->setEntity($this->getEntity());
            
            $criteria = new TCriteria;
            $criteria->add(new TFilter('id', '=', $this->id));
            
            $sql->setCriteria($criteria);
            
            foreach ($this->data as $key => $value){
                
                if($key !== 'id'){
                    
                    $sql->setRowData($key, $this->$key);
                    
                }
            }   
        }
    
        if($conn = TTransaction::get()){
            
            TTransaction::log($sql->getInstruction());
            
            $result = $conn->exec($sql->getInstruction());
            
            return $result;
            
        }else{
            
            throw new Exception('Não há transação ativa!!');
            
        }
    }       
    
    public function load($id){
        
        $sql = new TSqlSelect;
        $sql->setEntity($this->getEntity());
        $sql->addColumn('*');
        
        $criteria = new TCriteria;
        $criteria->add(new TFilter('id', '=', $id));
        $sql->setCriteria($criteria);
        
        if($conn = TTransaction::get()){
            
            TTransaction::log($sql->getInstruction());
            
            $result = $conn->Query($sql->getInstruction());
            
            if($result){
                
                $object = $result->fetchObject(get_class($this));
                
            }
            
            return $object;
            
        }else{
            
            throw new Exception('Não há transação ativa!!');
            
        } 
    }  
    
    public function loadLike($dado){
        
        $sql = new TSqlSelect;
        $sql->setEntity($this->getEntity());
        $sql->addColumn('*');
        
        $criteria = new TCriteria;
        $criteria->add(new TFilter('nome', 'LIKE', "%{$dado}%"));
        $sql->setCriteria($criteria);
        
        if($conn = TTransaction::get()){
            
            TTransaction::log($sql->getInstruction());
            
            $result = $conn->Query($sql->getInstruction());
            
                if($result){
                
                while($row = $result->fetch()){
                    
                    $results[] = $row;
                    
                }              
            }
                        
           return $results;
            
        }else{
            
            throw new Exception('Não há transação ativa!!');
            
        } 
    }       
    
    public function loadAll(){
        
        $sql = new TSqlSelect;
        $sql->setEntity($this->getEntity());
        $sql->addColumn('*');
                
        if($conn = TTransaction::get()){
            
            TTransaction::log($sql->getInstruction());
            
            $result = $conn->Query($sql->getInstruction());
            $results =array();
            
            if($result){
                
                while($row = $result->fetchObject(get_class($this))){
                    
                    $results[] = $row;
                    
                }              
            }
                        
            return $results;
            
        }else{
            
            throw new Exception('Não há transação ativa!!');
            
        } 
    }       
    
  
           
    public function delete($id = NULL){
        
        $id = $id ? $id : $this->id;
        
        $sql = new TSqlDelete;
        $sql->setEntity($this->getEntity());
        
        $criteria = new TCriteria;
        $criteria->add(new TFilter('id', '=', $id));
        $sql->setCriteria($criteria);
        
        if($conn = TTransaction::get()){
            
            TTransaction::log($sql->getInstruction());
            
            $result = $conn->exec($sql->getInstruction());
                    
            return $result;
            
        }else{
            
            throw new Exception('Não há transação ativa!!');
            
        } 
    }
}