<?php

namespace SON\Db\Config;

class TLoggerTXT extends TLogger{
    
        
    public function write($message){
        
        date_default_timezone_set('America/Sao_Paulo');
        $time = date("d-m-Y H:i:s");
        
        $text = "$time :: $message\r\n \r\n";
        $handler = fopen($this->filename, 'a');
        fwrite($handler, $text);
        fclose($handler);
    }    
}

?>