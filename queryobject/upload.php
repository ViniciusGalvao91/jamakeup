<?php
  
function __autoload($classe){
    
    if(file_exists("app.ado/{$classe}.class.php")){
        
        include_once "app.ado/{$classe}.class.php";
        
    }    
    
    if(file_exists("app.config/{$classe}.class.php")){
        
        include_once "app.config/{$classe}.class.php";
        
    }    
}

    $nomeevento = $_POST['nome_evento'];
    $descricaoevento = $_POST['descricao_evento'];
    
    $imagem = $_FILES['imagem']['tmp_name'];
    $tamanho = $_FILES['imagem']['size'];
    $tipo = $_FILES['imagem']['type'];
    $nome = $_FILES['imagem']['name'];

     if ( $imagem != "none" ){
         
      $fp = fopen($imagem, "rb");
      $conteudo = fread($fp, $tamanho);
      $conteudo = addslashes($conteudo);
      fclose($fp);
     }else{
         echo "FALHOU!!";
     }
        try{

            TTransaction::open('jamakeup');

            TTransaction::setLogger(new TLoggerTXT('tmp/arquivo.txt'));

            TTransaction::log("Salvando Imagem");

            //insert na base
            $sql = new TSqlInsert;
            $sql->setEntity('tabela_imagens');
            $sql->setRowData('evento', $nomeevento);
            $sql->setRowData('descricao', $descricaoevento);
            $sql->setRowData('nome_imagem', $imagem);
            $sql->setRowData('tamanho_imagem', $tamanho);
            $sql->setRowData('tipo_imagem', $tipo);
            $sql->setRowData('imagem', $conteudo);

            $conn = TTransaction::get();    
            $result = $conn->Query($sql->getInstruction());

            TTransaction::log($sql->getInstruction());    

            TTransaction::close();

        }catch(Exception $e){

            print " ERRO: " . $e->getMessage() . "<br>\n";
            TTransaction::rollback();
        }
    