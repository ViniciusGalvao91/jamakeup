<?php

final class TConnection{
    
    
    private function __construct(){
        
                
    }
    
    public static function open($name){
        
        if(file_exists("app.config/{$name}.ini")){
            
            $db = parse_ini_file("app.config/{$name}.ini");
            
        }else{
            
            throw new Exception("Arquivo '$name' não encontrado");
            
        }
        
        $user = isset($db['user']) ? $db['user'] : NULL;
        $pass = isset($db['pass']) ? $db['pass'] : NULL;
        $name = isset($db['name']) ? $db['name'] : NULL;
        $host = isset($db['host']) ? $db['host'] : NULL;
        $type = isset($db['type']) ? $db['type'] : NULL;
        $port = isset($db['port']) ? $db['port'] : NULL;
                
        $port = $port ? $port : '3306';
        $conn = new PDO("mysql:host={$host};port={$port};dbname={$name}", $user, $pass);
                                
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }    
}

?>